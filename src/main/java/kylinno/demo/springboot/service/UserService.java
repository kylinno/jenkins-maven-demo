package kylinno.demo.springboot.service;


import kylinno.demo.springboot.domain.User;

import java.util.List;

public interface UserService {

    User save(User user);

    List<User> getList();

}
