package kylinno.demo.springboot.repository;

import kylinno.demo.springboot.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {
}
