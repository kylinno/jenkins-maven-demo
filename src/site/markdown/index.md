简介
============
集成了SpringBoot的demo，Mvn站点需要的插件，jenkins与Maven集成的环境。

配置
=============
Java 1.8 +
Spring boot 1.3.5.Release
Maven 3.1.0
Jenkins 2.4
